# Sensor Demo

## Pre requisites:

- ROS Melodic or ROS Noetic

## Setup

1. Clone repository
```
$ cd ~
$ git clone https://gitlab.com/perceptionengine/pe-simulation/lidar_sample.git lidar_sample_ws/src/lidar_sample
$ cd lidar_sample_ws
```

2. Install dependencies:
```
$ rosdep install --from-paths src --ignore-src
```

3. Compile using catkin tools, catkin_make or colcon

catkin tools:
```
$ catkin build
```

catkin_make:
```
$ catkin_make
```

colcon:
```
$ colcon build
```

4. Source Workspace and launch sample
```
$ source devel/setup.bash # or install/setup.bash in the case of colcon
```

5. Launch
```
$ roslaunch pe_simulation_demo demo.launch 
```